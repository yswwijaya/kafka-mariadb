package main

import (
	"encoding/json"
	"fmt"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type DbPointer struct {
	Db *gorm.DB
}

type Job struct {
	ID          int    `gorm:"primary_key"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Company     string `json:"company"`
	Salary      string `json:"salary"`
}

func (Job) TableName() string {
	return "job"
}

// DBInit comment
func DBInit() *gorm.DB {

	connectionString := "root:pass@tcp(192.168.225.113:3306)/test?charset=utf8mb4&parseTime=True&loc=Local"

	db, err := gorm.Open("mysql", connectionString)
	if err != nil {
		fmt.Println(err.Error())
		panic("failed to connect to database")
	}

	return db
}

func (db Job) GetUserId() int {
	return db.ID
}

func main() {

	receiveFromKafka()

}

func receiveFromKafka() {

	db := &DbPointer{Db: DBInit()}

	fmt.Println("Start receiving from Kafka")
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": "localhost:9092",
		"group.id":          "group-id-1",
		"auto.offset.reset": "earliest",
	})

	if err != nil {
		panic(err)
	}

	c.SubscribeTopics([]string{"jobs-topic1"}, nil)

	for {
		msg, err := c.ReadMessage(-1)

		if err == nil {
			fmt.Printf("Received from Kafka %s: %s\n", msg.TopicPartition, string(msg.Value))
			job := string(msg.Value)
			db.saveJobToDB(job)
		} else {
			fmt.Printf("Consumer error: %v (%v)\n", err, msg)
			break
		}
	}

	c.Close()

}

func (dbp *DbPointer) saveJobToDB(jobString string) {

	tx := dbp.Db.Begin()

	fmt.Println("Save to MongoDB")

	//Save data into Job struct
	var _job Job
	b := []byte(jobString)
	err := json.Unmarshal(b, &_job)
	if err != nil {
		panic(err)
	}

	insert := &Job{
		Title:       _job.Title,
		Description: _job.Description,
		Company:     _job.Company,
		Salary:      _job.Salary,
	}

	if err := tx.Create(&insert).Error; err != nil {
		tx.Rollback()
	}

	userId := insert.GetUserId()

	tx.Commit()

	fmt.Printf("Saved to Database : %s, record ID : %d", jobString, userId)

}
